import React, {Component} from "react";
import {render} from "react-dom";
import TodoInput from './TodoInput';
import TodoList from './TodoList';
import '../css/style.css';
import uuid from 'uuid';

class TodoItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            todoArr: []
        }
    }

    getTodos = () => {
        fetch('http://localhost:3000/api/todos')
        .then(response => response.json())
        .then(myJson => this.setState({todoArr: myJson}));
    }
    componentDidMount(){
        this.getTodos();
    }

    removeTask = (id) => {
        fetch('http://localhost:3000/api/todos/'+id, {
            method: 'DELETE',
        })
        .then(response => response.json())
        .catch(error => console.error(`Fetch Error =\n`, error))
        .then(this.getTodos());
    }

    checkIt = (id, checkedVal) => {
        // let arr = this.state.todoArr;
        // let index = arr.findIndex(x => x.id === id);
        // arr[index].chk = checkedVal;

        let chkObj={
            _id:id,
            chk:checkedVal
        }
// console.log(chkObj)

        fetch('http://localhost:3000/api/todos/'+id, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(chkObj),
        })
        .then(response => response.json())
        .then(this.getTodos())
        .catch(error => console.error(`Fetch Error =\n`, error));



        // this.setState({todoArr: arr});
    }
    handleSubmit = (inputVal) => {
          let todoObj={
                _id:uuid.v4(),
                item:inputVal,
                chk:false
            }
       
            fetch('http://localhost:3000/api/todos', {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(todoObj),
               
            })
            .then(response => response.json())
            .then(this.getTodos())
            .catch(error => console.error(`Fetch Error =\n`, error));
            
    }
    render() {
        return ( 
        <div>
            <TodoInput addTask = {this.handleSubmit}/>
            <TodoList setTodo = {this.state.todoArr}
            remove = {this.removeTask}
            checker = {this.checkIt}/> 
        </div>
        );
    }
}

export default TodoItem;
render( <TodoItem/> , document.getElementById('todoContainer'));


    