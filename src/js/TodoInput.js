import React from 'react';

export default function TodoInput(props){

 let handleSubmit = (event) => {
    event.preventDefault();
    let inputVal = document.getElementById("text").value
    if(inputVal!=""){
        props.addTask(inputVal);
    }else{
        alert("please provide some input");
    }
    document.getElementById("text").value = "";
} 
        return (
            <form onSubmit={handleSubmit} >
            <input id="text" placeholder="input here"/>
            <input id="submit" type="submit"/>
            </form>
        )
}  
