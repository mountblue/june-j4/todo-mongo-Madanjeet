import React from 'react';

export default function TodoList(props) {
    let deleteTask = (id) => {
       props.remove(id)
    }

    let checkIt = (id, e) => {
        let checkedVal = e.target.checked;
        props.checker(id, checkedVal)
    }

    let itemArr;
    if(props.setTodo){
        itemArr=props.setTodo.map(items => {
            let cls = items.chk ? "check" : "uncheck";
            return(
                <li key={items._id}>
                <input className="chkBox" type="checkBox" onChange={e => checkIt(items._id, e)} checked={items.chk}/>
                <p className={cls}>{items.item}</p>
                <span className="cross" onClick={e => deleteTask(items._id,e)}>X</span>
                </li>
            )
        })
    }
        return(
            <ol className="todolist">{itemArr}</ol>
        )
}