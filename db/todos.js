const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let todoSchema = new Schema({
    _id:"string",
    item: "string",
    chk: "boolean"
})
let TodoModel= mongoose.model('todos',todoSchema);
module.exports = TodoModel


module.exports.getTotalTodos = function(callback){
    TodoModel.find(callback);
}