var express = require('express');
var app = new express();
const mongoose = require('mongoose');
const mongodb = "mongodb://localhost:27017/tododb";
const bodyParser = require('body-parser');
mongoose.connect(mongodb,{useNewUrlParser: true});

app.use(bodyParser.json());


app.get('/',function(req,res){
    res.sendFile(__dirname+'/dist/index.html');
})
.use(express.static('dist'));

app.use('/api',require('./routes/api'));


app.listen('3000');
console.log("Listen 3000")