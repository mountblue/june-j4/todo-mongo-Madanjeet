const express = require('express');
const router = express.Router();

const todolist = require('../db/todos.js');

router.get('/todos', function (req, res, next) {
    todolist.getTotalTodos(function (err, todosList) {
        res.json(todosList)
    })
})

router.post("/todos", function (req, res, next) {
    todolist.create(req.body).then(function (data) {
        res.send(data);
    })
})

router.put("/todos/:id", function (req, res, next) {
    // console.log(req.body._id+"........."+req.body.chk)
    todolist.findByIdAndUpdate({_id:req.body._id},{chk:req.body.chk}).then(function (data) {
        res.send(data);
    })
})


router.delete("/todos/:id", function (req, res, next) {
    todolist.findByIdAndRemove({_id:req.params.id}).then(function (data) {
        res.send(data);
    })
})

module.exports = router;